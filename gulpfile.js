const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */
elixir(function(mix) {
    mix.styles([
        'bootstrap.min.css',
        'animate.min.css',
        'ionicons.min.css',
        'styles.css'
    ]);
});
elixir(function(mix) {
    mix.scripts([
        'jquery.min.js',
        'bootstrap.min.js',
        'jquery.easing.min.js',
        'wow.js',
        'scripts.js'
    ]);
});
