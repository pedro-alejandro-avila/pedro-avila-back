<?php

namespace App\Matrix;


class Matrix
{
    public function fillMatrix($data)
    {
        $matrix[][][]=array();
        $result=$this->obtainData($data,$matrix);
        return $result;
    }

    private function obtainData($data,$matrix)
    {
        $result=array();
        $query = explode("\r\n", $data);
        $examples = explode(" ", trim($query[0]));
        $nmValues = explode(" ", trim($query[1]));
        $oldNmValue = 0;

        for ($j = 1; $j <= $examples[0]; $j++) {
            $matrix=$this->fillDefault( $matrix , $nmValues[0]);
            for ($i = $oldNmValue + 2; $i <= $nmValues[1] + $oldNmValue + 1; $i++) {
                $statement = explode(" ", $query[$i]);
                $matrix=$this->validateOrder($statement,$matrix);
                if($matrix[1]!== null) {
                    $result[] = $matrix[1];
                }
                $matrix=$matrix[0];
            }
            $oldNmValue = $i - 1;
            if ($j < $examples[0]) {
                $nmValues = explode(" ", trim($query[$i]));
            }
        }
        return json_encode($result);
    }

    private function validateOrder(array $statement,$matrix)
    {
        $summation=null;
        if($statement[0] == 'QUERY')
        {
            for($i=$statement[1];$i<=$statement[4];$i++)
            {
                for($j=$statement[2];$j<=$statement[5];$j++)
                {
                    for($k=$statement[3];$k<=$statement[6];$k++)
                    {
                        $summation+= $matrix[$i][$j][$k];

                    }
                }
            }
        }
        if($statement[0] == 'UPDATE')
        {
            $matrix[$statement[1]][$statement[2]][$statement[3]]=$statement[4];
        }
        return array($matrix,$summation);
    }

    private function fillDefault( $matrix , $size)
    {
        for($i=1;$i<=$size;$i++)
        {
            for($j=1;$j<=$size;$j++)
            {
                for($k=1;$k<=$size;$k++)
                {
                    $matrix[$i][$j][$k]=0;
                }
            }
        }
        unset($matrix[0]);
        return $matrix;
    }

}