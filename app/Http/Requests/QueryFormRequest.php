<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QueryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $query = explode("\r\n", $this->get('single-query'));

            if (sizeof($query) >= 2) {

                $examples = explode(" ", trim($query[0]));
                $nmValues = explode(" ", trim($query[1]));
                $oldNmValue = 0;
                if ($this->validateNmValues($nmValues)) {
                    $validator->errors()->add('single-query', $this->validateNmValues($nmValues) . "2");
                }
                if ($this->validateExampleNumbers($examples)) {
                    $validator->errors()->add('single-query', $this->validateExampleNumbers($examples) . "1");
                }

                if (!$this->validateNmValues($nmValues) && !$this->validateExampleNumbers($examples)) {

                    for ($j = 1; $j <= $examples[0]; $j++) {
                        for ($i = $oldNmValue + 2; $i <= $nmValues[1] + $oldNmValue + 1; $i++) {
                            if ($i >= sizeof($query)) {
                                $validator->errors()->add("single-query", __('messages.not-coherent') . ($i + 1));
                                break;
                            }
                            $statement = explode(" ", $query[$i]);
                            $error = $this->cycling($statement, $nmValues);
                            if ($error) {
                                $validator->errors()->add('single-query', $error . " " . ($i + 1));
                            }
                        }
                        $oldNmValue = $i - 1;
                        if ($j < $examples[0]) {
                            if ($i + 2 > sizeof($query)) {
                                $validator->errors()->add("single-query", __('messages.not-coherent') . ($i + 1));
                                break;
                            }
                            $nmValues = explode(" ", trim($query[$i]));

                            if ($this->validateNmValues($nmValues)) {
                                $validator->errors()->add('single-query', $this->validateNmValues($nmValues) . " " . ($i + 1));
                                break;
                            }
                        }
                    }
                }
            }
            if (sizeof($query) < 2) {
                $validator->errors()->add('single-query', __('messages.not-valid'));
            }
        });
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return $data
     */
    private function cycling(array $statement, array $nmValues)
    {
        $data = null;
        if ($statement[0] != "QUERY" && $statement[0] != "UPDATE") {
            $data = __('messages.have-problem');
        }

        if ($statement[0] == "UPDATE") {
            if (sizeof($statement) != 5) {
                $data = __('incorrect-length-update');
            }
            if ($statement[sizeof($statement) - 1] > 1000000000) {
                $data = __('messages.incorrect-size-update');
            }
            if ($statement[sizeof($statement) - 1] < -1000000000) {
                $data = __('messages.incorrect-size-update-negative');
            }
            if (sizeof($statement == 5)) {
                $data = $this->validateCells($nmValues[0], $statement);
            }
            for ($i = 1; $i < sizeof($statement); $i++) {
                if (!is_numeric($statement[$i])) {
                    $data = __('messages.incorrect-notation-update');
                }
                if (is_numeric($statement[$i])) {
                    if ($statement[$i] < 1) {
                        if ($i < (sizeof($statement) - 1)) {
                            $data = __('messages.non-zero-update');
                        }
                    }
                }
            }
        }

        if ($statement[0] == "QUERY") {
            if (sizeof($statement) != 7) {
                $data = __('messages.incorrect-length-query');
            }
            for ($i = 1; $i < sizeof($statement); $i++) {
                if (!is_numeric($statement[$i])) {
                    $data = __('messages.incorrect-notation-query');
                }
                if (is_numeric($statement[$i])) {
                    if ($statement[$i] < 1) {
                        $data = __('messages.non-zero-query');
                    }
                }
            }
            if (sizeof($statement == 7)) {
                $data = $this->validateCellsQuery($nmValues[0], $statement);
            }
        }

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return $data
     */
    private function validateNmValues(array $values)
    {
        $data = null;
        if (sizeof($values) != 2) {
            $data = __('messages.incorrect-notation-nm');
        }
        if (!is_numeric($values[0])) {
            $data = __('messages.only-numbers');
        }
        if (sizeof($values) == 2) {
            if (is_numeric($values[0]) && is_numeric($values[1])) {
                if ($values[0] < 1 || $values[1] < 1) {
                    $data = __('messages.non-zero');
                }
                if ($values[0] > 100) {
                    $data = __('messages.maximum-cube');
                }
                if ($values[1] > 1000) {
                    $data = __('messages.maximum-operations');
                }
                if (!is_numeric($values[1])) {
                    $data = __('messages.only-numbers');
                }
            }
        }
        if ($data) {
            $data .= __('messages.verify-at');
        }
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return $data
     */
    private function validateExampleNumbers(array $values)
    {
        $data = null;
        if (sizeof($values) != 1) {
            $data = __('messages.cases-number-notation');
        }
        if (!is_numeric($values[0])) {
            $data = __('messages.only-numbers-line');
        }
        if (is_numeric($values[0])) {
            if ($values[0] > 50) {
                $data = __('messages.incorrect-cases-number');
            }
            if ($values[0] < 1) {
                $data = __('messages.non-zero-line');
            }
        }

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return $data
     */
    private function validateCells($nmValues, array $statement)
    {
        $data = null;
        if ($nmValues[0] < $statement[1]) {
            $data = __('messaGes.size-exceeded-update');
        }
        if ($nmValues[0] == $statement[1]) {
            if ($statement[2] > $statement[1]) {
                $data = __('messages.size-exceeded-update');
            }
            if ($statement[3] > $statement[1]) {
                $data = __('messages.size-exceeded-update');
            }
        }
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return $data
     */
    private function validateCellsQuery($nmValues, array $statement)
    {
        $data = null;
        if ($nmValues[0] < $statement[1]) {
            $data = __('messages.size-exceeded-query');
        }
        if ($nmValues[0] == $statement[1]) {
            if ($statement[2] > $statement[1]) {
                $data = __('messages.size-exceeded-query');
            }
            if ($statement[3] > $statement[1]) {
                $data = __('messages.size-exceeded-query');
            }
        }

        if ($nmValues[0] < $statement[4]) {
            $data = __('messages.size-exceeded-query');
        }
        if ($nmValues[0] == $statement[1]) {
            if ($statement[5] > $statement[1]) {
                $data = __('messages.size-exceeded-query');
            }
            if ($statement[6] > $statement[1]) {
                $data = __('messages.size-exceeded-query');
            }
        }

        if ($statement[1] > $statement[4]) {
            $data = __('messages.cant-query');
        }
        if ($statement[1] == $statement[4]) {
            if ($statement[2] > $statement[5]) {
                $data = __('messages.cant-query');
            }
            if ($statement[2] == $statement[5]) {
                if ($statement[3] > $statement[6]) {
                    $data = __('messages.cant-query');
                }
            }

        }


        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'single-query' => 'required',
        ];
    }
}
