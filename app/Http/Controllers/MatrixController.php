<?php

namespace App\Http\Controllers;

use App\Http\Requests\QueryFormRequest;
use App\Matrix\Matrix;
use Illuminate\Http\Request;

class MatrixController extends Controller
{


    /**
     * Create a new matrix from a executed and previously verified query.
     *
     * @param  array $data
     * @return boolean
     */
    protected function create()
    {
        return view('cube');
    }

    public function query(QueryFormRequest $request)
    {
        $query = $request->get('single-query');
        $matrix = new Matrix();
        $solve = $matrix->fillMatrix($query);

        return \Redirect::route('query')
            ->with('message', __('messages.query-result') . $solve);
    }

}
