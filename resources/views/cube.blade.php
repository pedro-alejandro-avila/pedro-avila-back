<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cube Summation</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{!! asset('css/all.css') !!}" media="all" rel="stylesheet" type="text/css"/>

</head>
<body>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-4">
            @if(Session::has('message'))
                <div class="alert alert-success">
                    {{Session::get('message')}}
                </div>
            @endif
            <h1>Query "Cube Summation"</h1>
            <h2>Conditions of the problem</h2>
           <h4>Input Format</h4>
            <h6>The first line contains an integer T, the number of test-cases. T testcases follow.
            For each test case, the first line will contain two integers N and M separated by a single space.
            N defines the N * N * N matrix.
            M defines the number of operations.
            The next M lines will contain either</h6>
            <img src="http://i.imgur.com/l7cye3u.png" alt="">
        </div>
        <div class="col-md-2">
            <h2>Capa Modelo</h2>
            <p>Esta Capa contiene la clase Matrix.php
                donde se realizan las operaciones con
                los datos que llegan de la query que
                se envía desde el controlador</p>
        </div>
        <div class="col-md-2">
            <h2>Capa Vista</h2>
            <p>En esta capa se encuentra la vista llamada
               cube.blade.php, esta se ocupa de recolectar los
                datos, y asismísmo mostrar los resultados del
                modelo.
            </p>
        </div>
        <div class="col-md-2">
            <h2>Capa Controlador</h2>
            <p>Esta Capa contiene la clase MatrixController.php
                este se encarga de recibir y enviar los datos de
                la capa del modelo y enviarlos adecuadamente a la
                vista que correspondan.
            </p>
        </div>
        <div class="col-md-2">
            <h2>'http-Request'</h2>
            <p>Aquí se contiene la clase QueryFormRequest.php
                esta se encarga de recibir y validar los datos antes
                de enviarlos al controlador.
            </p>
        </div>
    </div>
</div>
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
<div class="row">
    <div class="col-md-12">
        <div class="col-sm-4">
            {!! Form::open(array('route' => 'lets_query', 'class' => 'form')) !!}
            <div class="form-group">
                {!! Form::label('Your Query') !!}
                {!! Form::textarea('single-query', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Your Query',
                          'style'=>'height:150px')) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Let\'s Query!',
                  array('class'=>'btn btn-primary')) !!}
            </div>
            {!! Form::close() !!}
        </div>

        <div class="col-sm-3">
            <h3>QueryFormRequest.php</h3>
            <p>La responsabilidad de esta clase es la validación
                sintáctica y léxica de la entrada que llega, en
                este caso la query sobre la que se trabaja.
                Por ejemplo, se valida que esté bien escrita la
                palabra 'QUERY'
            </p>
        </div>


        <div class="col-sm-3">
            <h3>MatrixController.php</h3>
            <p>Esta clase gestiona los cambios que se realicen
                desde Matrix.php y actualiza la vista con los
                resultados de las operaciones del modelo.
            </p>
        </div>

        <div class="col-sm-2">
            <h3>Matrix.php</h3>
            <p>
                Esta clase se encarga de las operaciones segun
                la query que se envíe, genera los datos de la
                matriz y opera con ellos.
            </p>
        </div>

    </div>
</div>

</body>
<script type="text/javascript" src="{!! asset('js/all.js') !!}"></script>
</html>