<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'not-coherent'                      => 'The number of operations not are coherent with the query size line: ',
    'have-problem'                      => 'Verify your query, you Have a Problem in the line: ',
    'not-valid'                         => 'The query is not valid ',
    'incorrect-length-update'           => 'The UPDATE does not have the correct length in line: ',
    'incorrect-size-update'             => 'The cell value cant exceed the 1000000000 at line: ',
    'incorrect-size-update-negative'    => 'The cell value cant exceed the -1000000000 at line: ',
    'incorrect-notation-update'         => 'The UPDATE does not have the correct notation in line: ',
    'non-zero-update'                   => 'The UPDATE number must be greater than 0 in line: ',
    'incorrect-length-query'            => 'The QUERY does not have the correct length in line: ',
    'incorrect-notation-query'          => 'The QUERY does not have the correct notation in line: ',
    'non-zero-query'                    => 'The QUERY number must be greater than 0 in line: ',
    'incorrect-notation-nm'             => 'The notation of the line is not correct,',
    'only-numbers'                      => 'Please use numbers only, ',
    'non-zero'                          => 'The number must be greater than 0,',
    'maximum-cube'                      => 'The maximum value of the cube is 100, ',
    'maximum-operations'                => 'The maximum value of operations for any cube is 1000, ',
    'verify-at'                         => 'verify it at line: ',
    'cases-number-notation'             => 'Please verify the notation of the number of cases in line: ',
    'only-numbers-line'                 => 'Please use only numbers in the line: ',
    'incorrect-cases-number'            => 'The max value of cases is 50 , please verify line: ',
    'non-zero-line'                     => 'The number must be greater than 0 in line: ',
    'size-exceed-update'                => 'The cell that you trying to UPDATE exceed the size of the Cube at line: ',
    'size-exceed-query'                 => 'The cell that you trying to QUERY exceed the size of the Cube at line: ',
    'cant-query'                        => 'The first value is greater than the second line: ',
    'query-result'                      => 'The result of the query is : ',
];
